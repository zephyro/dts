<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <div class="alert_line">
                <div class="container">
                    <a href="#">Zurück in eigene Account</a>
                </div>
            </div>

            <?php include('inc/header.man.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__col">
                            <ul class="breadcrumb">
                                <li><span>Statistik</span></li>
                            </ul>
                        </div>
                        <div class="heading__col"></div>
                    </div>
                    
                    <div id="app">

                        <div class="selection">
                            <div class="selection__elem">
                                <div class="date_interval">
                                    <div class="date_interval__label">Datum von</div>
                                    <div class="date_interval__content">
                                        <div class="date_interval__input">
                                            <date-picker type="date" size="small" confirm placeholder="Select date"></date-picker>
                                        </div>
                                        <div class="date_interval__text">bis</div>
                                        <div class="date_interval__input">
                                            <date-picker type="date" size="small" confirm placeholder="Select date"></date-picker>
                                        </div>
                                    </div>
                                    <div class="date_interval__button">
                                        <i-button size="small" shape="circle">Absenden</i-button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="alert_box alert_box_white mb_30">
                            <div class="alert_box__icon">
                                <img src="img/icon__alert_check.svg" class="img-fluid" alt="">
                            </div>
                            <div class="alert_box__content">
                                <h4>ERFOLG</h4>
                                <div class="alert_box__text">Benutzername und Passwort korrekt eingegeben</div>
                            </div>
                        </div>

                        <div class="alert_box alert_box_purple mb_30">
                            <span class="alert_box__close"></span>
                            <div class="alert_box__icon">
                                <img src="img/icon__alert_purple.svg" class="img-fluid" alt="">
                            </div>
                            <div class="alert_box__content">
                                <h4>ACHTUNG</h4>
                                <div class="alert_box__text">Der eingegebene Benutzername oder Passwort sind nicht korrekt. Passwort erneut eingeben, Zum Entsperren hier <a href="#">klicken</a></div>
                                <div class="alert_box__date">Sperrdatum: 10/10/2020; 15:32:13</div>
                            </div>
                        </div>

                        <div class="data">
                            <ul class="data__views">
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="25" checked="">
                                        <span><i>25</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="50">
                                        <span><i>50</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="100">
                                        <span><i>100</i></span>
                                    </label>
                                </li>
                            </ul>
                            <div class="data__table">
                                <i-table
                                        :columns="columns"
                                        :data="data" ref="table">

                                    <template slot-scope="{ row }" slot="name">
                                        <a class="table_link" v-if="row['url']" :href="row['url']" v-text="row['name']"></a>
                                        <span v-else v-text="row['name']"></span>
                                    </template>

                                    <template slot-scope="{ row }" slot="limit">
                                        <span v-text="row['limit']"></span>
                                    </template>
                                    <template slot-scope="{ row }" slot="request">
                                        <span v-text="row['request']"></span>
                                    </template>

                                    <template slot-scope="{ row }" slot="count">
                                        <span v-text="row['count']"></span>
                                    </template>

                                    <template slot-scope="{ row }" slot="success">
                                        <span v-text="row['success']"></span>
                                    </template>

                                    <template slot-scope="{ row }" slot="failure">
                                        <span v-text="row['failure']"></span>
                                    </template>

                                    <template slot-scope="{ row }" slot="unknown">
                                        <span v-text="row['unknown']"></span>
                                    </template>

                                </i-table>
                            </div>

                            <page :total="100" />

                        </div>

                    </div>

                </div>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            var table = {
                data () {
                    return {
                        columns: [
                            {
                                title: "User",
                                key: "name",
                                Width : 180,
                                minWidth: 180,
                                fixed: "left",
                                sortable: true,
                                slot: "name",
                            },
                            {
                                title: "Datum",
                                key: "date",
                                minWidth: 200,
                                sortable: true
                            },
                            {
                                slot: 'limit',
                                title: "Tageslimit Vodafone",
                                key: "limit",
                                className: "text_center",
                                width: 200,
                                sortable: true,
                                renderHeader: h => h('span', [
                                    h('span', {
                                        class: 'text_base'
                                    }, 'Tageslimit Vodafone'),
                                    h('span', {
                                        class: 'th_value',
                                    }, 33),
                                ]),
                            },
                            {
                                title: "Anfragen Vodafone",
                                key: "request",
                                className: "text_center",
                                width: 180,
                                sortable: true,
                                slot: 'request',
                                renderHeader: h => h('span', [
                                    h('span', {
                                        class: 'text_base'
                                    }, 'Anfragen Vodafone'),
                                    h('span', {
                                        class: 'th_value',
                                    }, 33),
                                ]),
                            },
                            {
                                title: "Gesamtanzahl",
                                key: "count",
                                className: "text_center",
                                width: 160,
                                sortable: true,
                                slot: 'count',
                                renderHeader: h => h('span', [
                                    h('span', {
                                        class: 'text_base'
                                    }, 'Gesamtanzahl'),
                                    h('span', {
                                        class: 'th_value',
                                    }, 33),
                                ]),
                            },
                            {
                                title: "Erflog",
                                key: "success",
                                className: "color_green text_center",
                                width: 100,
                                sortable: true,
                                slot: 'success',
                                renderHeader: h => h('span', [
                                    h('span', {
                                        class: 'text_base'
                                    }, 'Erflog'),
                                    h('span', {
                                        class: 'th_value',
                                    }, 33),
                                ]),
                            },
                            {
                                title: "Absage",
                                key: "failure",
                                className: "color_purple text_center",
                                width: 120,
                                sortable: true,
                                slot: 'failure',
                                renderHeader: h => h('span', [
                                    h('span', {
                                        class: 'text_base'
                                    }, 'Absage'),
                                    h('span', {
                                        class: 'th_value',
                                    }, 33),
                                ]),
                            },
                            {
                                title: "Unbekannt",
                                key: "unknown",
                                className: "color_brown text_center",
                                width: 150,
                                sortable: true,
                                slot: 'unknown',
                                renderHeader: h => h('span', [
                                    h('span', {
                                        class: 'text_base'
                                    }, 'Unbekannt'),
                                    h('span', {
                                        class: 'th_value',
                                    }, 33),
                                ]),
                            }
                        ],

                        data: [
                            {
                                "name": "Nutzer 1",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 198,
                                "count": 55,
                                "success": 10,
                                "failure": 30,
                                "unknown": 15,
                                "url": '#',
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Manager",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 120,
                                "request": 215,
                                "count": 42,
                                "success": 12,
                                "failure": 25,
                                "unknown": 11,
                                "url": '#',
                                cellClassName: {
                                    failure: 'bg_purple'

                                }
                            },
                            {
                                "name": "Nutzer 3",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 314,
                                "count": 28,
                                "success": 7,
                                "failure": 29,
                                "unknown": 18,
                                "url": '#',
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 4",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": "-/-",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                "url": '#',
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 5",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 512,
                                "count": 84,
                                "success": 31,
                                "failure": 28,
                                "unknown": 42,
                                "url": '#',
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 6",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 425,
                                "count": 81,
                                "success": 52,
                                "failure": 11,
                                "unknown": 24,
                                "url": '#',
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 7",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 358,
                                "count": 91,
                                "success": 54,
                                "failure": 11,
                                "unknown": 21,
                                "url": '#',
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 8",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 221,
                                "count": 32,
                                "success": 15,
                                "failure": 11,
                                "unknown": 18,
                                "url": '#',
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 9",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 328,
                                "count": 44,
                                "success": 21,
                                "failure": 14,
                                "unknown": 12,
                                "url": '#',
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            }
                        ],
                        model1: ''
                    }
                }
            };

            var component = Vue.extend(table);
            new component().$mount('#app');

        </script>

    </body>
</html>
