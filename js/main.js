
// User nav

(function() {

    $('.user__button').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.user').find('.user__nav').toggleClass('open');
    });

}());


// Hide dropdown

$('body').click(function (event) {

    if ($(event.target).closest(".user").length === 0) {
        $(".user__nav").removeClass('open');
    }
});



// Nav
(function() {

    $('.nav_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.header').toggleClass('nav_open');
    });

}());






$('.schedule_col').on('click', function() {

    var $currentTable = $(this).closest('table');
    var index = $(this).index();

    if ($(this).hasClass("selected")) {
        $(this).removeClass('selected');
        $currentTable.find('tr').each(function() {
            $(this).find('td').eq(index).find('input').prop("checked", false);
        });
    }

    else {
        $(this).addClass('selected');
        $currentTable.find('tr').each(function() {
            $(this).find('td').eq(index).find('input').prop("checked", true);
        });
    }
});



$('.schedule__table td:first-child').on('click', function() {

    var $currentRow = $(this).closest('tr');


    if ($(this).hasClass("selected")) {
        $(this).removeClass('selected');
        $currentRow.find('input').prop("checked", false);
    }

    else {
        $(this).addClass('selected');
        $currentRow.find('input').prop("checked", true);
    }
});