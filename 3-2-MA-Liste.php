<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <?php include('inc/header.man.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div  id="app">

                        <div class="heading heading_between">
                            <div class="heading__col">
                                <ul class="breadcrumb">
                                    <li><span>Liste</span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="selection">
                            <div class="selection__elem">
                                <div class="date_interval">
                                    <div class="date_interval__label">Datum von</div>
                                    <div class="date_interval__content">
                                        <div class="date_interval__input">
                                            <date-picker type="date" size="small" confirm placeholder="Select date"></date-picker>
                                        </div>
                                        <div class="date_interval__text">bis</div>
                                        <div class="date_interval__input">
                                            <date-picker type="date" size="small" confirm placeholder="Select date"></date-picker>
                                        </div>
                                    </div>
                                    <div class="date_interval__button">
                                        <i-button size="small" shape="circle">Absenden</i-button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="data mb_40">
                            <ul class="data__views">
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="25" checked="">
                                        <span><i>25</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="50">
                                        <span><i>50</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="100">
                                        <span><i>100</i></span>
                                    </label>
                                </li>
                            </ul>
                            <div class="data__table">
                                <i-table :columns="columns1" :data="data1" ref="table" ></i-table>
                            </div>
                        </div>

                    </div>

                </div>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            var table = {
                data () {
                    return {
                        columns1: [
                            {
                                "title": "Datum",
                                "key": "date",
                                "width": 210,
                                "fixed": "left",
                                "sortable": true
                            },
                            {
                                "title": "Tageslimit Vodafone",
                                "key": "limit",
                                "className": "text_center",
                                "minWidth": 180,
                                "sortable": false
                            },
                            {
                                "title": "Anfragen Vodafone",
                                "key": "request",
                                "className": "text_center",
                                "width": 180,
                                "sortable": false
                            },
                            {
                                "title": "Gesamtanzahl",
                                "key": "count",
                                "className": "text_center",
                                "width": 160,
                                "sortable": false
                            },
                            {
                                "title": "Erflog",
                                "key": "success",
                                "className": "color_green text_center",
                                "width": 100,
                                "sortable": false
                            },
                            {
                                "title": "Absage",
                                "key": "failure",
                                "className": "color_purple text_center",
                                "width": 120,
                                "sortable": false
                            },
                            {
                                "title": "Unbekannt",
                                "key": "unknown",
                                "className": "color_brown text_center",
                                "width": 150,
                                "sortable": false
                            }
                        ],
                        data1: [
                            {
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 198,
                                "count": 55,
                                "success": 10,
                                "failure": 30,
                                "unknown": 15,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 120,
                                "request": 215,
                                "count": 42,
                                "success": 12,
                                "failure": 25,
                                "unknown": 11,
                                cellClassName: {
                                    failure: 'bg_purple'

                                }
                            },
                            {
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 314,
                                "count": 28,
                                "success": 7,
                                "failure": 29,
                                "unknown": 18,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": "-/-",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 512,
                                "count": 84,
                                "success": 31,
                                "failure": 28,
                                "unknown": 42,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 425,
                                "count": 81,
                                "success": 52,
                                "failure": 11,
                                "unknown": 24,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 358,
                                "count": 91,
                                "success": 54,
                                "failure": 11,
                                "unknown": 21,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 221,
                                "count": 32,
                                "success": 15,
                                "failure": 11,
                                "unknown": 18,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 328,
                                "count": 44,
                                "success": 21,
                                "failure": 14,
                                "unknown": 12,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            }
                        ],
                        model1: ''
                    }
                }
            };

            var component = Vue.extend(table);
            new component().$mount('#app');

        </script>

        <script>

            new Chartist.Line('.ct-chart', {
                labels: [1, 2, 3, 4, 5, 6, 7, 8],
                series: [
                    [1, 2, 3, 1, -2, 0, 1, 0],
                    [-2, -1, -2, -1, -3, -1, -2, -1],
                    [0, 0, 0, 1, 2, 3, 2, 1],
                    [3, 2, 1, 0.5, 1, 0, -1, -3]
                ]
            }, {
                high: 3,
                low: -3,
                fullWidth: true,
                // As this is axis specific we need to tell Chartist to use whole numbers only on the concerned axis
                axisY: {
                    onlyInteger: true,
                    offset: 20
                },
            });


        </script>

    </body>
</html>
