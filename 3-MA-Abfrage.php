<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <?php include('inc/header.man.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div  id="app">

                        <div class="heading heading_between">
                            <div class="heading__col">
                                <ul class="breadcrumb">
                                    <li><span>Abfrage</span></li>
                                </ul>
                            </div>
                            <div class="heading__col">
                                <i-button type="primary">
                                    <icon type="md-add"></icon>
                                    MA Anlegen
                                </i-button>
                            </div>
                        </div>

                        <div class="white_box">
                            <div class="row">
                                <div class="col col-xs-12 col-sm-4 col-md-4 col-lg-3 col-xl-4 col-gutter-lr">
                                    <div class="form-group">
                                        <label class="form_label">Mobil</label>
                                        <div class="form_wrap">
                                            <i-input placeholder="+49 177 1234567"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-5 col-md-5 col-lg-6 col-xl-6 col-gutter-lr">
                                    <div class="form-group">
                                        <label class="form_label">Passwort</label>
                                        <div class="form_wrap">
                                            <i-input type="password" placeholder="**********"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-3 col-md-3  col-lg-3 col-xl-2 col-gutter-lr">
                                    <label class="form_label"></label>
                                    <i-button type="primary">Absenden</i-button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="content">
                        <div class="stat">
                            <div class="stat__info">
                                <h1>Meine Statistiken</h1>
                                <div class="stat__info_wrap">
                                    <div class="stat_switch mb_20">
                                        <label class="stat_switch__item">
                                            <input type="radio" name="stat_switch" value="1" checked>
                                            <span>6 monate</span>
                                        </label>
                                        <label class="stat_switch__item">
                                            <input type="radio" name="stat_switch" value="2">
                                            <span>30 tage</span>
                                        </label>
                                        <label class="stat_switch__item">
                                            <input type="radio" name="stat_switch" value="3">
                                            <span>7 tage</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="stat__heading stat__heading_purple">
                                    <span class="stat__heading_primary">Absage</span>
                                    <span class="stat__heading_second">120</span>
                                </div>

                                <table class="stat__table mb_20">
                                    <tr>
                                        <td>
                                            <span>Kein Interesse</span>
                                        </td>
                                        <td>
                                            <span>35</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Anbeiter wechseln</span>
                                        </td>
                                        <td>
                                            <span>25</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Prämien passt nicht</span>
                                        </td>
                                        <td>
                                            <span>15</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>Blacklist</span>
                                        </td>
                                        <td>
                                            <span>5</span>
                                        </td>
                                    </tr>
                                </table>

                                <div class="stat__heading stat__heading_green">
                                    <span class="stat__heading_primary">Erfolg</span>
                                    <span class="stat__heading_second">40</span>
                                </div>

                                <table class="stat__table mb_20">
                                    <tr>
                                        <td>
                                            <span>HW</span>
                                        </td>
                                        <td>
                                            <span>15</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span>GS</span>
                                        </td>
                                        <td>
                                            <span>25</span>
                                        </td>
                                    </tr>
                                </table>

                                <div class="stat__heading stat__heading_gray">
                                    <span class="stat__heading_primary">Unbekant</span>
                                    <span class="stat__heading_second">40</span>
                                </div>

                            </div>
                            <div class="stat__chart">
                                <div class="ct-chart"></div>
                                <div class="chart_legend">
                                    <span class="chart_legend_one">ERFOLG</span>
                                    <span class="chart_legend_two">Absage</span>
                                    <span class="chart_legend_three">UNBEKANT</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            var table = {
                data () {
                    return {
                        columns1: [
                            {
                                "title": "User",
                                "key": "name",
                                "Width" : 180,
                                "minWidth": 180,
                                "fixed": "left",
                                "sortable": true
                            },
                            {
                                "title": "Datum",
                                "key": "date",
                                "minWidth": 200,
                                "sortable": false
                            },
                            {
                                "title": "Tageslimit Vodafone",
                                "key": "limit",
                                "className": "text_center",
                                "width": 180,
                                "sortable": false
                            },
                            {
                                "title": "Anfragen Vodafone",
                                "key": "request",
                                "className": "text_center",
                                "width": 180,
                                "sortable": false
                            },
                            {
                                "title": "Gesamtanzahl",
                                "key": "count",
                                "className": "text_center",
                                "width": 160,
                                "sortable": false
                            },
                            {
                                "title": "Erflog",
                                "key": "success",
                                "className": "color_green text_center",
                                "width": 100,
                                "sortable": false
                            },
                            {
                                "title": "Absage",
                                "key": "failure",
                                "className": "color_purple text_center",
                                "width": 120,
                                "sortable": false
                            },
                            {
                                "title": "Unbekannt",
                                "key": "unknown",
                                "className": "color_brown text_center",
                                "width": 150,
                                "sortable": false
                            }
                        ],
                        data1: [
                            {
                                "name": "Nutzer 1",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 198,
                                "count": 55,
                                "success": 10,
                                "failure": 30,
                                "unknown": 15,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Manager",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 120,
                                "request": 215,
                                "count": 42,
                                "success": 12,
                                "failure": 25,
                                "unknown": 11,
                                cellClassName: {
                                    failure: 'bg_purple'

                                }
                            },
                            {
                                "name": "Nutzer 3",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 314,
                                "count": 28,
                                "success": 7,
                                "failure": 29,
                                "unknown": 18,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 4",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": "-/-",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 5",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 512,
                                "count": 84,
                                "success": 31,
                                "failure": 28,
                                "unknown": 42,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 6",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 425,
                                "count": 81,
                                "success": 52,
                                "failure": 11,
                                "unknown": 24,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 7",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 358,
                                "count": 91,
                                "success": 54,
                                "failure": 11,
                                "unknown": 21,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 8",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 221,
                                "count": 32,
                                "success": 15,
                                "failure": 11,
                                "unknown": 18,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            },
                            {
                                "name": "Nutzer 9",
                                "date": "'15/11/2019 - 20/11/2019",
                                "limit": 200,
                                "request": 328,
                                "count": 44,
                                "success": 21,
                                "failure": 14,
                                "unknown": 12,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline',
                                    failure: 'bg_purple'
                                }
                            }
                        ],
                        model1: ''
                    }
                }
            };

            var component = Vue.extend(table);
            new component().$mount('#app');

        </script>

        <script>

            new Chartist.Line('.ct-chart', {
                labels: [1, 2, 3, 4, 5, 6, 7, 8],
                series: [
                    [1, 2, 3, 1, -2, 0, 1, 0],
                    [-2, -1, -2, -1, -3, -1, -2, -1],
                    [0, 0, 0, 1, 2, 3, 2, 1],
                    [3, 2, 1, 0.5, 1, 0, -1, -3]
                ]
            }, {
                high: 3,
                low: -3,
                fullWidth: true,
                // As this is axis specific we need to tell Chartist to use whole numbers only on the concerned axis
                axisY: {
                    onlyInteger: true,
                    offset: 20
                },
            });


        </script>

    </body>
</html>
