<!doctype html>
<html class="no-js" lang="">

<head>
    <?php include('inc/head.inc.php') ?>
</head>

<body>

<div class="page">

    <?php include('inc/header.inc.php') ?>

    <section class="main">
        <div class="container">

            <div class="heading">
                <div class="heading__col">
                    <ul class="breadcrumb">
                        <li><a href="#">VP</a></li>
                        <li><span>Editieren</span></li>
                    </ul>
                </div>
                <div class="heading__col"></div>
            </div>


            <div id="app">

                <div class="content">

                    <div class="content__group">

                        <div class="inline mb_20">
                            <div class="inline__left"></div>
                            <div class="inline__right">
                                <h2>VP Bearbeiten</h2>
                            </div>
                        </div>

                        <div class="inline form_group">
                            <div class="inline__left">
                                <label class="form_label">Name VP<span class="color_red">*</span></label>
                            </div>
                            <div class="inline__right">
                                <div class="form_wrap">
                                    <i-input placeholder=""/>
                                </div>
                            </div>
                        </div>

                        <div class="inline form_group">
                            <div class="inline__left">
                                <label class="form_label">Tageslimit Vodafone<span class="color_red">*</span></label>
                            </div>
                            <div class="inline__right">
                                <div class="form_wrap">
                                    <i-input placeholder=""/>
                                </div>
                            </div>
                        </div>

                        <div class="inline form_group">
                            <div class="inline__left">
                                <label class="form_label">Firmenadresse<span class="color_red">*</span></label>
                            </div>
                            <div class="inline__right">
                                <div class="form_wrap">
                                    <i-input placeholder=""/>
                                </div>
                            </div>
                        </div>

                        <div class="inline form_group">
                            <div class="inline__left">
                                <label class="form_label">Kundendaten anzeigen wenn VVL nicht möglich*</label>
                            </div>
                            <div class="inline__right">
                                <div class="form_wrap">
                                    <checkbox></checkbox>
                                </div>
                            </div>
                        </div>

                        <div class="inline form_group">
                            <div class="inline__left">
                                <label class="form_label">Kundendaten anzeigen wenn VVL nicht möglich*</label>
                            </div>
                            <div class="inline__right">
                                <div class="form_wrap">
                                    <checkbox></checkbox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="inline mb_20">
                        <div class="inline__left">
                        </div>
                        <div class="inline__right">
                            <div class="text_right">
                                <div class="btn_group btn_group_right">
                                    <div class="btn_group__elem">
                                        <i-button type="text">Reset</i-button>
                                    </div>
                                    <div class="btn_group__elem">
                                        <i-button type="primary" size="large">Anlegen</i-button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>

            </div>

        </div>

    </section>


    <?php include('inc/footer.inc.php') ?>

</div>

<?php include('inc/scripts.inc.php') ?>


<script>

    var Main = {
        data () {
            return {
                formLeft: {
                    input1: '',
                    input2: '',
                    input3: ''
                },
                formRight: {
                    input1: '',
                    input2: '',
                    input3: ''
                },
                formTop: {
                    input1: '',
                    input2: '',
                    input3: ''
                }
            }
        }
    }

    var Component = Vue.extend(Main)
    new Component().$mount('#app')



</script>

</body>

</html>