<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script type="text/javascript" src="js/vendor/iview/iview.min.js"></script>
<script type="text/javascript" src="js/vendor/iview/locale/ru-RU.js"></script>
<script type="text/javascript" src="js/vendor/chartist/chartist.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>