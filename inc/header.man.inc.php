<header class="header">
    <div class="container">
        <div class="header__row">
            <div class="header__user"><a href="#">User@email.ru</a> | Manager</div>
            <a class="header__logo" href="/">
                <img src="img/logo.svg" class="img-fluid" alt="">
            </a>
            <div class="header__content">
                <nav class="header__nav">
                    <ul>
                        <li class="active"><a href="#">Statistik</a></li>
                        <li><a href="#">MA</a></li>
                        <li class="disable"><a href="#">Tarifen</a></li>

                    </ul>
                </nav>
                <div class="user">
                    <div class="user__button">
                        <img src="img/header__user_icon.svg" class="img-fluid" alt="">
                    </div>
                    <div class="user__nav">
                        <ul>
                            <li><a href="#">
                                    <i><img src="img/icon__exit.svg" class="ico-svg" alt=""></i>
                                    <span>Exit</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <a class="header__toggle nav_toggle" href="#">
                    <span></span>
                </a>
            </div>
        </div>
        <div class="header__info">
            <ul>
                <li class="active main_tooltip" data-tooltip="Unbekant">185</li>
                <li class="main_tooltip" data-tooltip="Unbekant">50</li>
                <li class="main_tooltip" data-tooltip="Unbekant">50</li>
                <li class="main_tooltip color_green" data-tooltip="Unbekant">15</li>
                <li class="main_tooltip color_purple" data-tooltip="Unbekant">25</li>
                <li class="main_tooltip color_brown" data-tooltip="Unbekant">10</li>
            </ul>
            <span class="info_time">~60 sek</span>
        </div>
    </div>
</header>