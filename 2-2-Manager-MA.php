<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <?php include('inc/header.man.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__col">
                            <ul class="breadcrumb">
                                <li><span>MA</span></li>
                            </ul>
                        </div>
                        <div class="heading__col"></div>
                    </div>

                    <div id="app">

                        <div class="form_inline form_inline_right mb_20">
                            <div class="form_inline__elem form_inline__elem_btn">
                                <i-button type="primary">
                                    <icon type="md-add"></icon>
                                    MA Anlegen
                                </i-button>
                            </div>
                        </div>

                        <div class="data">
                            <ul class="data__views">
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="25" checked="">
                                        <span><i>25</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="50">
                                        <span><i>50</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="100">
                                        <span><i>100</i></span>
                                    </label>
                                </li>
                            </ul>
                            <div class="data__table">
                                <i-table
                                        :columns="columns"
                                        :data="data"
                                        ref="table" >

                                    <template slot-scope="{ row }" slot="id">
                                        <div class="table_flex">
                                            <i class="table_icon">
                                                <img :src="row['image']" alt="">
                                            </i>
                                            <span v-text="row['id']"></span>
                                        </div>
                                    </template>

                                    <template slot-scope="{ row }" slot="name">
                                        <a class="table_link" v-if="row['url']" :href="row['url']" v-text="row['name']"></a>
                                        <span v-else v-text="row['name']"></span>
                                    </template>

                                </i-table>
                            </div>

                            <page :total="100" />

                        </div>

                    </div>

                </div>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>


            var table = {
                data () {
                    return {
                        columns: [
                            {
                                title: "ID",
                                key: "id",
                                fixed: "left",
                                "width": 110,
                                sortable: true,
                                slot: "id"
                            },
                            {
                                title: "Name",
                                key: "name",
                                "minWidth" : 180,
                                sortable: true,
                                slot: "name"
                            },
                            {
                                title: "VP",
                                key: "vp",
                                "minWidth" : 120,
                                sortable: false
                            },
                            {
                                title: "Level",
                                key: "level",
                                "minWidth" : 120,
                                "className": "text_center",
                                sortable: false
                            },
                            {
                                title: "Tageslimit  Vodafone",
                                key: "limit",
                                "className": "text_center",
                                "width": 180,
                                sortable: false
                            },
                            {
                                title: "Anfragen Vodafone",
                                key: "request",
                                "className": "text_center bg_rose",
                                "width": 180,
                                sortable: false
                            }
                        ],
                        data: [
                            {
                                "id": 129,
                                "name": "Vidofone-admin",
                                "vp": "'VP 1",
                                "level": "user",
                                "limit": "198",
                                "request": 198,
                                "url": '#',
                                "image": 'img/icon__tie.svg'
                            },
                            {
                                "id": 130,
                                "name": "Vidofone-admin",
                                "vp": "'VP 1",
                                "level": "user",
                                "limit": "'198",
                                "request": 215,
                                "url": '#',
                                "image": 'img/icon__tie.svg'
                            },
                            {
                                "id": 132,
                                "name": "VP 1-admin",
                                "vp": "'VP 1",
                                "level": "Manager",
                                "limit": "'198",
                                "request": 314,
                                "url": '#',
                                "image": 'img/icon__headphone.svg'
                            },
                            {
                                "id": 133,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "level": "Manager",
                                "limit": "'198",
                                "request": 315,
                                "url": '#',
                                "image": 'img/icon__headphone.svg'
                            },
                            {
                                "id": 187,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "level": "user",
                                "limit": "'198",
                                "request": 315,
                                "image": 'img/icon__headphone.svg'
                            },
                            {
                                "id": 138,
                                "name": "MA 6",
                                "vp": "'VP 1",
                                "level": "user",
                                "limit": "'198",
                                "request": 512,
                                "url": '#',
                                "image": 'img/icon__headphone.svg'
                            },
                            {
                                "id": 152,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "level": "user",
                                "limit": "'198",
                                "request": 315,
                                "url": '#',
                                "image": 'img/icon__headphone.svg'
                            },
                            {
                                "id": 154,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "level": "user",
                                "limit": "'198",
                                "request": 315,
                                "url": '#',
                                "image": 'img/icon__headphone.svg'
                            },
                            {
                                "id": 159,
                                "name": "MA 4",
                                "level": "user",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 315,
                                "url": '#',
                                "image": 'img/icon__headphone.svg'
                            },
                            {
                                "id": 161,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "level": "user",
                                "limit": "'198",
                                "request": 315,
                                "url": '#',
                                "image": 'img/icon__headphone.svg'
                            },
                        ],
                        model1: ''
                    }
                }
            };


            var component = Vue.extend(table);
            new component().$mount('#app');



        </script>

    </body>
</html>
