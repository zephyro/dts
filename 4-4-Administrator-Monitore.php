<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div id="app">

                        <div class="heading">
                            <div class="heading__col">
                                <ul class="breadcrumb">
                                    <li><span>Monitor</span></li>
                                </ul>
                            </div>
                            <div class="heading__col">
                                <i-button type="primary">Anlegen</i-button>
                            </div>
                        </div>

                        <div class="data">
                            <ul class="data__views data__views_text">
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="Alle">
                                        <span><i>Alle</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="Blockiert"  checked="">
                                        <span><i>Blockiert</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="Activ">
                                        <span><i>Activ</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="Aufgebraucht">
                                        <span><i>Aufgebraucht</i></span>
                                    </label>
                                </li>
                            </ul>
                            <div class="data__table">
                                <i-table
                                        :columns="columns"
                                        :data="data" ref="table">

                                    <template slot-scope="{ row }" slot="status">
                                        <div class="table_flex">
                                            <i class="table_icon">
                                                <img :src="row['image']" alt="">
                                            </i>
                                            <span v-text="row['name']"></span>
                                        </div>
                                    </template>


                                    <template slot-scope="{ row }" slot="name">
                                        <a class="table_link" v-if="row['url']" :href="row['url']" v-text="row['name']"></a>
                                        <span v-else v-text="row['name']"></span>
                                    </template>

                                    <template slot-scope="{ row }" slot="progress">
                                        <span class="table_progress">
                                            <span :data-val="row['pr_min']" :data-total="row['data-progress']" v-text="row['progress']"></span>
                                            <i style="width: 60px;"></i>
                                        </span>
                                    </template>


                                    <template slot-scope="{ row }" slot="check">
                                        <label class="main_checkbox">
                                            <input type="checkbox"  :name="row['check']">
                                            <span></span>
                                        </label>
                                    </template>

                                </i-table>
                            </div>
                        </div>


                        <div class="text_right" style="padding-top: 30px">
                            <dropdown>
                                <i-button type="info">
                                    Aktion ausführen
                                    <icon type="ios-arrow-down"></icon>
                                </i-button>
                                <dropdown-menu slot="list">
                                    <dropdown-item>Block</dropdown-item>
                                    <dropdown-item>Entsperren</dropdown-item>
                                </dropdown-menu>
                            </dropdown>
                        </div>

                    </div>

                </div>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            var table = {
                data () {
                    return {
                        columns: [
                            {
                                title: "Status",
                                key: "status",
                                "width": 220,
                                "minWidth": 220,
                                fixed: "left",
                                sortable: true,
                                slot: "status",
                            },
                            {
                                title: " ",
                                key: "value",
                                "width": 60,
                                className: "text_center",
                                sortable: true
                            },
                            {
                                title: "Name",
                                key: "name",
                                "minWidth": 180,
                                sortable: true,
                                slot: "name",
                            },
                            {
                                title: "Provider",
                                key: "provider",
                                "minWidth": 200,
                                className: "text_center",
                                sortable: true
                            },
                            {
                                title: " ",
                                key: "progress",
                                "width": 180,
                                className: "text_center",
                                sortable: false,
                                slot: "progress",
                            },
                            {
                                title: " ",
                                key: "check",
                                "width": 100,
                                className: "text_center",
                                sortable: false,
                                slot: "check"
                            }
                        ],

                        data: [
                            {
                                "status": "Vom Admin gesperrt",
                                "value": "20",
                                "name": "Lebara-C0a",
                                "provider": "Vodafone",
                                "progress": "60/160",
                                "data-progress": "38",
                                "check": " ",
                                "url": '#',
                                "image": 'img/table_icon__clock.svg'
                            },
                            {
                                "status": "Vom Admin gesperrt",
                                "value": "5",
                                "name": "OrtelApp-NISSI",
                                "provider": "Vodafone",
                                "progress": "0/1200",
                                "data-progress": "0",
                                "check": " ",
                                "url": '#',
                                "image": 'img/table_icon__hand.svg'
                            },
                            {
                                "status": "Limit überschritten",
                                "value": "9",
                                "name": "PartosBlauworld",
                                "provider": "Vodafone",
                                "progress": "60/100",
                                "data-progress": "60",
                                "check": " ",
                                "url": '#',
                                "image": 'img/table_icon__lock.svg'
                            },
                            {
                                "status": "Zeitbegrenzung",
                                "value": "8",
                                "name": "PartosBlauworld",
                                "provider": "Vodafone",
                                "progress": "100/100",
                                "data-progress": "100",
                                "check": " ",
                                "url": '#',
                                "image": 'img/table_icon__lock2.svg'
                            },
                            {
                                "status": "Zugangsdaten falsch",
                                "value": "4",
                                "name": "luca-hundler-Kios",
                                "provider": "Vodafone",
                                "progress": "1309/1310",
                                "data-progress": "100",
                                "check": " ",
                                "url": '#',
                                "image": 'img/table_icon__pause.svg'
                            },
                        ],

                        model1: ''
                    }
                }
            };

            var component = Vue.extend(table);
            new component().$mount('#app');

        </script>

    </body>
</html>
