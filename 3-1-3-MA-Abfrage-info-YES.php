<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <?php include('inc/header.man.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div  id="app">

                        <div class="heading heading_between">
                            <div class="heading__col">
                                <ul class="breadcrumb">
                                    <li><span>Abfrage</span></li>
                                </ul>
                            </div>

                        </div>

                        <div class="top_link">
                            <a href="#">Datenbank abfrage</a>
                        </div>

                        <div class="white_box">
                            <div class="progress">
                                <div class="progress__title">Im Progress...</div>
                                <div class="progress__content">
                                    <i-progress :percent="50" :stroke-width="30" status="active"/>
                                </div>
                            </div>
                        </div>

                        <div class="white_box">
                            <div class="row">
                                <div class="col col-xs-12 col-sm-4 col-md-4 col-lg-3 col-xl-4 col-gutter-lr">
                                    <div class="form-group">
                                        <label class="form_label">Mobil</label>
                                        <div class="form_wrap">
                                            <i-input placeholder="+49 177 1234567"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-5 col-md-5 col-lg-6 col-xl-6 col-gutter-lr">
                                    <div class="form-group">
                                        <label class="form_label">Passwort</label>
                                        <div class="form_wrap">
                                            <i-input type="password" placeholder="**********"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-3 col-md-3  col-lg-3 col-xl-2 col-gutter-lr">
                                    <label class="form_label"></label>
                                    <i-button type="primary">Absenden</i-button>
                                </div>
                            </div>
                        </div>

                        <div class="white_box">
                            <div class="row">
                                <div class="col col-xs-12 col-sm-4 col-md-4 col-lg-3 col-xl-4 col-gutter-lr">
                                    <div class="form-group">
                                        <label class="form_label">Mobil</label>
                                        <div class="form_wrap">
                                            <i-select v-model="model1" placeholder=" ">
                                                <i-option v-for="item in list" :value="item.value" :key="item.value">{{ item.label }}</i-option>
                                            </i-select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-5 col-md-5 col-lg-6 col-xl-6 col-gutter-lr">
                                    <div class="form-group">
                                        <label class="form_label">Kommentare</label>
                                        <div class="form_wrap">
                                            <i-input type="text" placeholder=""/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xs-12 col-sm-3 col-md-3  col-lg-3 col-xl-2 col-gutter-lr">
                                    <label class="form_label"></label>
                                    <i-button type="warning">Absenden</i-button>
                                </div>
                            </div>
                        </div>

                        <div class="text_center mb_30">
                            <i-button type="success">Success</i-button>
                        </div>
                        <div class="text_center mb_30">
                            <i-button type="default">Default</i-button>
                        </div>
                        <div class="text_center mb_30">
                            <i-button type="primary">Primary</i-button>
                        </div>
                        <div class="text_center mb_30">
                            <i-button type="error">Error</i-button>
                        </div>
                        <div class="text_center mb_30">
                            <i-button type="warning">Warning</i-button>
                        </div>
                    </div>

                    <div class="alert_box alert_box_green mb_30">
                        <div class="alert_box__icon">
                            <img src="img/icon__goods.svg" class="img-fluid" alt="">
                        </div>
                        <div class="alert_box__content">
                            <h4 class="text-uppercase">Kunde abfragen</h4>
                            <div class="alert_box__text color_green">Standard VVL moglich ab 16.05.2019</div>
                        </div>
                    </div>

                    <div class="alert_box alert_box_purple mb_30">
                        <span class="alert_box__close"></span>
                        <div class="alert_box__icon">
                            <img src="img/icon__alert_purple.svg" class="img-fluid" alt="">
                        </div>
                        <div class="alert_box__content">
                            <h4>ACHTUNG</h4>
                            <div class="alert_box__text">Der eingegebene Benutzername oder Passwort sind nicht korrekt. Passwort erneut eingeben, Zum Entsperren hier <a href="#">klicken</a></div>
                            <div class="alert_box__date">Sperrdatum: 10/10/2020; 15:32:13</div>
                        </div>
                    </div>

                    <div class="alert_box alert_box_purple mb_30">
                        <div class="alert_box__icon">
                            <img src="img/icon__stop.svg" class="img-fluid" alt="">
                        </div>
                        <div class="alert_box__content">
                            <h4>ACHTUNG</h4>
                            <div class="alert_box__text">Der eingegebene Benutzername oder Passwort sind nicht korrekt. Passwort erneut eingeben, Zum Entsperren hier <a href="#">klicken</a></div>
                            <div class="alert_box__date">Sperrdatum: 10/10/2020; 15:32:13</div>
                        </div>
                    </div>

                    <div class="content">

                        <div class="content__heading mb_40">
                            <h2 class="text_light">Kunde abfragen</h2>
                            <p >Durchschnittliche Abfragezeit in der letzten Stunde: Sek.</p>
                        </div>

                        <div class="row">
                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">

                                <div class="user_info">

                                    <ul class="user_info__meta">
                                        <li class="color_green">Standard VVL moglich ab 16.05.2019</li>
                                    </ul>

                                    <h4 class="mb_10">Neue Abfrage</h4>

                                    <table class="info_table">
                                        <tr>
                                            <td>Erstellt </td>
                                            <td>17.05.2019 17:12:08</td>
                                        </tr>
                                        <tr>
                                            <td>Nummer</td>
                                            <td>01722879010</td>
                                        </tr>
                                        <tr>
                                            <td>Passwort</td>
                                            <td>sonja</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Kunde</td>
                                            <td>Frau Monika Avermann</td>
                                        </tr>
                                        <tr>
                                            <td>Adresse</td>
                                            <td>Lindener Str. 41, 44879 Bochum</td>
                                        </tr>
                                        <tr>
                                            <td>Region</td>
                                            <td>alle Regionen</td>
                                        </tr>
                                        <tr>
                                            <td>Tarif</td>
                                            <td>Smart M 2013 m. Basic Phone</td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>Datensatz vollständig</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>nächster Erwerb</td>
                                            <td>22.07.2021</td>
                                        </tr>
                                        <tr>
                                            <td>letzter Erwerb</td>
                                            <td>23.04.2019</td>
                                        </tr>
                                        <tr>
                                            <td>Vertragsbeginn</td>
                                            <td>22.07.2019</td>
                                        </tr>
                                        <tr>
                                            <td>Vertragsende</td>
                                            <td>21.07.2021</td>
                                        </tr>
                                        <tr>
                                            <td>Kartenanzahl</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>IBAN</td>
                                            <td>****************2669</td>
                                        </tr>
                                        <tr>
                                            <td>BIC</td>
                                            <td>PBNKDEFFXXX</td>
                                        </tr>
                                        <tr>
                                            <td>Kontoinh</td>
                                            <td>Monika Avermann</td>
                                        </tr>
                                        <tr>
                                            <td>Umsatz</td>
                                            <td>16 EUR</td>
                                        </tr>
                                    </table>

                                    <div class="user_info__divider mb_30"></div>

                                    <h4 class="mb_10">mgl. Tarife</h4>

                                    <div class="select_box" style="height: 210px;">
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Unterdr. Rufnr. def. erlaubt</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Vodafone-Mailbox</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Vodafone-Message</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Flag VVlberecht. Kd.f.Pfomoss</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Rabatt auf Basispreis 0,5 Euro</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1" checked>
                                            <span>VF Group</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Rufnummernanzeige</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Mehrwertd. Vodafone-Tarife</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Nutzung JOYN-Dienst möglich</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Standarddienste</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>GPRS Default</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1" checked>
                                            <span>VF Group</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Rufnummernanzeige</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Mehrwertd. Vodafone-Tarife</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Nutzung JOYN-Dienst möglich</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Standarddienste</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>GPRS Default</span>
                                        </label>
                                    </div>

                                </div>

                            </div>
                            <div class="col col-xs-12 col-sm-6 col-gutter-lr">

                                <div class="user_service">

                                    <h4 class="mb_10">NBA-Tipps:</h4>

                                    <ul class="point_list mb_30">
                                        <li>Info-Tipp: keine Tarifempfehlung</li>
                                        <li>SOHO-Angebot VVL mit Red S Smartphone (Sub15)</li>
                                        <li>Info-Tipp: keine Tarifempfehlung</li>
                                        <li>SOHO-Angebot VVL mit Red S Smartphone (Sub15)</li>
                                        <li>Info-Tipp: keine Tarifempfehlung</li>
                                        <li>SOHO-Angebot VVL mit Red S Smartphone (Sub15)</li>
                                        <li>Info-Tipp: keine Tarifempfehlung</li>
                                        <li>SOHO-Angebot VVL mit Red S Smartphone (Sub15)</li>
                                    </ul>

                                    <h4 class="mb_10">Spezial-Tipps:</h4>
                                    <ul class="point_list mb_30">
                                        <li>Info-Tipp: keine Tarifempfehlung</li>
                                        <li>SOHO-Angebot VVL mit Red S Smartphone (Sub15)</li>
                                    </ul>

                                    <h4 class="mb_10">Dienste</h4>

                                    <div class="select_box" style="height: 210px;">
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Unterdr. Rufnr. def. erlaubt</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Vodafone-Mailbox</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Vodafone-Message</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Flag VVlberecht. Kd.f.Pfomoss</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Rabatt auf Basispreis 0,5 Euro</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1" checked>
                                            <span>VF Group</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Rufnummernanzeige</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Mehrwertd. Vodafone-Tarife</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Nutzung JOYN-Dienst möglich</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Standarddienste</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>GPRS Default</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Flag VVlberecht. Kd.f.Pfomoss</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Rabatt auf Basispreis 0,5 Euro</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1" checked>
                                            <span>VF Group</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Rufnummernanzeige</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Mehrwertd. Vodafone-Tarife</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Nutzung JOYN-Dienst möglich</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>Standarddienste</span>
                                        </label>
                                        <label class="select_box__item">
                                            <input type="radio" name="r1" value="1">
                                            <span>GPRS Default</span>
                                        </label>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            var table = {
                data () {
                    return {
                        list: [
                            {
                                value: 'VP - Alle',
                                label: 'VP - Alle'
                            },
                            {
                                value: 'VP - 1',
                                label: 'VP - 1'
                            },
                            {
                                value: 'VP - 2',
                                label: 'VP - 2'
                            },
                            {
                                value: 'VP - 3',
                                label: 'VP - 3'
                            },
                            {
                                value: 'VP - 4',
                                label: 'VP - 4'
                            },
                            {
                                value: 'VP - 5',
                                label: 'VP - 5'
                            }
                        ],
                        model1: ''
                    }
                }
            };

            var component = Vue.extend(table);
            new component().$mount('#app');

        </script>

    </body>
</html>
