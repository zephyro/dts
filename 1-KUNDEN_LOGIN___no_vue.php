<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body  class="auth_bg">

        <div class="auth" id="app">

            <div class="auth__wrap">

                <div class="auth__logo">
                    <img src="img/logo_large.svg" class="img-fluid" alt="">
                </div>

                <div class="auth__form">
                    <div class="auth__form_wrap">
                        <div class="auth__title">
                            <span>KUNDEN LOGIN</span>
                        </div>
                        <form autocomplete="off" class="ivu-form ivu-form-label-right">
                            <div class="ivu-form-item"><!---->
                                <div class="form_wrap__error">Error: text message</div>
                                <div class="ivu-form-item-content">
                                    <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type ivu-input-group ivu-input-group-default ivu-input-group-with-prepend">
                                        <div class="ivu-input-group-prepend" style="">
                                            <i class="ivu-icon ivu-icon-ios-mail"></i>
                                        </div> <!---->
                                        <i class="ivu-icon ivu-icon-ios-loading ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                        <input autocomplete="off" spellcheck="false" type="text" placeholder="Email" class="ivu-input ivu-input-default"> <!---->
                                    </div> <!---->
                                </div>
                            </div>
                            <div class="ivu-form-item"><!---->
                                <div class="ivu-form-item-content">
                                    <div class="ivu-input-wrapper ivu-input-wrapper-default ivu-input-type ivu-input-group ivu-input-group-default ivu-input-group-with-prepend">
                                        <div class="ivu-input-group-prepend" style="">
                                            <i class="ivu-icon ivu-icon-ios-lock-outline"></i>
                                        </div> <!---->
                                        <i class="ivu-icon ivu-icon-ios-loading ivu-load-loop ivu-input-icon ivu-input-icon-validate"></i>
                                        <input autocomplete="off" spellcheck="false" type="password" placeholder="Password" class="ivu-input ivu-input-default"> <!---->
                                    </div> <!---->
                                </div>
                            </div>
                            <div class="ivu-form-item"><!---->
                                <div class="ivu-form-item-content">
                                    <button type="button" class="ivu-btn ivu-btn-primary"><!----> <!----> <span>login</span></button> <!---->
                                </div>
                            </div>
                        </form>
                        <div class="auth__error">Login nicht möglich</div>
                    </div>
                </div>

            </div>

        </div>

        <?php include('inc/scripts.inc.php') ?>


    </body>
</html>
