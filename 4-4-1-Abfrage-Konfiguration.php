<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__col">
                            <ul class="breadcrumb">
                                <li><a href="#">Monitor</a></li>
                                <li><span>Anlegen Konfiguration</span></li>
                            </ul>
                        </div>
                        <div class="heading__col"></div>
                    </div>


                    <div id="app">

                        <div class="content">

                            <div class="content__group">

                                <div class="inline mb_20">
                                    <div class="inline__left"></div>
                                    <div class="inline__right">
                                        <h4>Konfiguration</h4>
                                    </div>
                                </div>

                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <label class="form_label">Name Konfiguration</label>
                                    </div>
                                    <div class="inline__right">
                                        <div class="form_wrap">
                                            <i-input placeholder="" value="Lebara-C0dobraa"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="inline mb_20">
                                    <div class="inline__left"></div>
                                    <div class="inline__right">
                                        <h4>Einstellungen</h4>
                                    </div>
                                </div>

                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <label class="form_label">Höchstanzahl pro Tag</label>
                                    </div>
                                    <div class="inline__right">
                                        <div class="form_wrap" style="width: 100px;">
                                            <i-input placeholder="" value="50"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <label class="form_label">Anzahl heute</label>
                                    </div>
                                    <div class="inline__right">
                                        <div class="form_wrap" style="width: 100px;">
                                            <i-input placeholder=""/>
                                        </div>
                                    </div>
                                </div>

                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <label class="form_label">Priorität</label>
                                    </div>
                                    <div class="inline__right">
                                        <div class="form_wrap" style="width: 100px;">
                                            <i-input placeholder="" value="40"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <label class="form_label">Pause</label>
                                    </div>
                                    <div class="inline__right">
                                        <div class="row middle-xs">
                                            <div class="col col-xs-4 col-gutter-lr">
                                                <div class="form_wrap">
                                                    <i-input placeholder="" value="50"/>
                                                </div>
                                            </div>
                                            <div class="col col-xs-4 col-gutter-lr">
                                                <div class="form_wrap">
                                                    <i-input placeholder="" value="50"/>
                                                </div>
                                            </div>
                                            <div class="col col-xs-4 col-gutter-lr">
                                                <span>sekunden</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="inline inline_top form-group">
                                    <div class="inline__left">
                                        <label class="form_label">Verfügbar</label>
                                    </div>
                                    <div class="inline__right">
                                        <radio-group v-model="vertical" vertical>
                                            <radio label="Jetzt">
                                                <span>Jetzt</span>
                                            </radio>
                                            <div class="inline_form">
                                                <radio label="">
                                                    <span></span>
                                                </radio>
                                                <div class="form_wrap">
                                                    <date-picker type="date" confirm placeholder="00/00/1988; --:--"></date-picker>
                                                </div>
                                            </div>
                                        </radio-group>
                                    </div>
                                </div>

                            </div>

                            <div class="content__group mb_40">

                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <div>Zulässige Tagen und Öffnungszeiten</div>
                                    </div>
                                    <div class="inline__right">

                                    </div>
                                </div>

                                <div class="schedule">
                                    <table class="schedule__table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>0</th>
                                            <th>1</th>
                                            <th>2</th>
                                            <th>3</th>
                                            <th>4</th>
                                            <th>5</th>
                                            <th>6</th>
                                            <th>7</th>
                                            <th>8</th>
                                            <th>9</th>
                                            <th>10</th>
                                            <th>11</th>
                                            <th>12</th>
                                            <th>13</th>
                                            <th>14</th>
                                            <th>15</th>
                                            <th>16</th>
                                            <th>17</th>
                                            <th>18</th>
                                            <th>19</th>
                                            <th>20</th>
                                            <th>21</th>
                                            <th>22</th>
                                            <th>23</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        <tr>
                                            <td><span class="schedule__weekday">So</span> </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 00:00-00:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="0" data-position="1" value="1" class="sr-only">
                                                    <span>0</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 01:00-01:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="1" data-position="2" value="2" class="sr-only">
                                                    <span>1</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 02:00-02:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="2" data-position="3" value="3" class="sr-only">
                                                    <span>2</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 03:00-03:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="3" data-position="4" value="4" class="sr-only">
                                                    <span>3</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 04:00-04:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="4" data-position="5" value="5" class="sr-only">
                                                    <span>4</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 05:00-05:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="5" data-position="6" value="6" class="sr-only">
                                                    <span>5</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 06:00-06:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="6" data-position="7" value="7" class="sr-only">
                                                    <span>6</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 07:00-07:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="7" data-position="8" value="8" class="sr-only">
                                                    <span>7</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 08:00-08:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="8" data-position="9" value="9" class="sr-only">
                                                    <span>8</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 09:00-09:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="9" data-position="10" value="10" class="sr-only">
                                                    <span>9</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 10:00-10:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="10" data-position="11" value="11" class="sr-only">
                                                    <span>10</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 11:00-11:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="11" data-position="12" value="12" class="sr-only">
                                                    <span>11</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 12:00-12:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="12" data-position="13" value="13" class="sr-only">
                                                    <span>12</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 13:00-13:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="13" data-position="14" value="14" class="sr-only">
                                                    <span>13</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 14:00-14:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="14" data-position="15" value="15" class="sr-only">
                                                    <span>14</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 15:00-15:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="15" data-position="16" value="16" class="sr-only">
                                                    <span>15</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 16:00-16:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="16" data-position="17" value="17" class="sr-only">
                                                    <span>16</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 17:00-17:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="17" data-position="18" value="18" class="sr-only">
                                                    <span>17</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 18:00-18:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="18" data-position="19" value="19" class="sr-only">
                                                    <span>18</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 19:00-19:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="19" data-position="20" value="20" class="sr-only">
                                                    <span>19</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 20:00-20:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="20" data-position="21" value="21" class="sr-only">
                                                    <span>20</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 21:00-21:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="21" data-position="22" value="22" class="sr-only">
                                                    <span>21</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 22:00-22:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="22" data-position="23" value="23" class="sr-only">
                                                    <span>22</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="So 23:00-23:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="23" data-position="24" value="24" class="sr-only">
                                                    <span>23</span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="schedule__weekday">Mo</span></td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 00:00-00:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="0" data-position="1" value="1" class="sr-only">
                                                    <span>0</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 01:00-01:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="1" data-position="2" value="2" class="sr-only">
                                                    <span>1</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 02:00-02:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="2" data-position="3" value="3" class="sr-only">
                                                    <span>2</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 03:00-03:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="3" data-position="4" value="4" class="sr-only">
                                                    <span>3</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 04:00-04:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="4" data-position="5" value="5" class="sr-only">
                                                    <span>4</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 05:00-05:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="5" data-position="6" value="6" class="sr-only">
                                                    <span>5</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 06:00-06:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="6" data-position="7" value="7" class="sr-only">
                                                    <span>6</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 07:00-07:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="7" data-position="8" value="8" class="sr-only">
                                                    <span>7</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 08:00-08:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="8" data-position="9" value="9" class="sr-only">
                                                    <span>8</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 09:00-09:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="9" data-position="10" value="10" class="sr-only">
                                                    <span>9</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 10:00-10:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="10" data-position="11" value="11" class="sr-only">
                                                    <span>10</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 11:00-11:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="11" data-position="12" value="12" class="sr-only">
                                                    <span>11</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 12:00-12:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="12" data-position="13" value="13" class="sr-only">
                                                    <span>12</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 13:00-13:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="13" data-position="14" value="14" class="sr-only">
                                                    <span>13</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 14:00-14:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="14" data-position="15" value="15" class="sr-only">
                                                    <span>14</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 15:00-15:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="15" data-position="16" value="16" class="sr-only">
                                                    <span>15</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 16:00-16:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="16" data-position="17" value="17" class="sr-only">
                                                    <span>16</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 17:00-17:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="17" data-position="18" value="18" class="sr-only">
                                                    <span>17</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 18:00-18:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="18" data-position="19" value="19" class="sr-only">
                                                    <span>18</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 19:00-19:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="19" data-position="20" value="20" class="sr-only">
                                                    <span>19</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 20:00-20:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="20" data-position="21" value="21" class="sr-only">
                                                    <span>20</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 21:00-21:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="21" data-position="22" value="22" class="sr-only">
                                                    <span>21</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 22:00-22:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="22" data-position="23" value="23" class="sr-only">
                                                    <span>22</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mo 23:00-23:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="23" data-position="24" value="24" class="sr-only">
                                                    <span>23</span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="schedule__weekday">Di</span></td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 00:00-00:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="0" data-position="1" value="1" class="sr-only">
                                                    <span>0</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 01:00-01:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="1" data-position="2" value="2" class="sr-only">
                                                    <span>1</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 02:00-02:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="2" data-position="3" value="3" class="sr-only">
                                                    <span>2</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 03:00-03:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="3" data-position="4" value="4" class="sr-only">
                                                    <span>3</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 04:00-04:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="4" data-position="5" value="5" class="sr-only">
                                                    <span>4</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 05:00-05:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="5" data-position="6" value="6" class="sr-only">
                                                    <span>5</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 06:00-06:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="6" data-position="7" value="7" class="sr-only">
                                                    <span>6</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 07:00-07:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="7" data-position="8" value="8" class="sr-only">
                                                    <span>7</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 08:00-08:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="8" data-position="9" value="9" class="sr-only">
                                                    <span>8</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 09:00-09:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="9" data-position="10" value="10" class="sr-only">
                                                    <span>9</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 10:00-10:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="10" data-position="11" value="11" class="sr-only">
                                                    <span>10</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 11:00-11:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="11" data-position="12" value="12" class="sr-only">
                                                    <span>11</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 12:00-12:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="12" data-position="13" value="13" class="sr-only">
                                                    <span>12</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 13:00-13:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="13" data-position="14" value="14" class="sr-only">
                                                    <span>13</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 14:00-14:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="14" data-position="15" value="15" class="sr-only">
                                                    <span>14</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 15:00-15:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="15" data-position="16" value="16" class="sr-only">
                                                    <span>15</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 16:00-16:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="16" data-position="17" value="17" class="sr-only">
                                                    <span>16</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 17:00-17:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="17" data-position="18" value="18" class="sr-only">
                                                    <span>17</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 18:00-18:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="18" data-position="19" value="19" class="sr-only">
                                                    <span>18</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 19:00-19:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="19" data-position="20" value="20" class="sr-only">
                                                    <span>19</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 20:00-20:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="20" data-position="21" value="21" class="sr-only">
                                                    <span>20</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 21:00-21:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="21" data-position="22" value="22" class="sr-only">
                                                    <span>21</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 22:00-22:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="22" data-position="23" value="23" class="sr-only">
                                                    <span>22</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Di 23:00-23:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="23" data-position="24" value="24" class="sr-only">
                                                    <span>23</span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="schedule__weekday">Mi</span></td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 00:00-00:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="0" data-position="1" value="1" class="sr-only">
                                                    <span>0</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 01:00-01:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="1" data-position="2" value="2" class="sr-only">
                                                    <span>1</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 02:00-02:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="2" data-position="3" value="3" class="sr-only">
                                                    <span>2</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 03:00-03:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="3" data-position="4" value="4" class="sr-only">
                                                    <span>3</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 04:00-04:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="4" data-position="5" value="5" class="sr-only">
                                                    <span>4</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 05:00-05:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="5" data-position="6" value="6" class="sr-only">
                                                    <span>5</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 06:00-06:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="6" data-position="7" value="7" class="sr-only">
                                                    <span>6</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 07:00-07:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="7" data-position="8" value="8" class="sr-only">
                                                    <span>7</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 08:00-08:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="8" data-position="9" value="9" class="sr-only">
                                                    <span>8</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 09:00-09:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="9" data-position="10" value="10" class="sr-only">
                                                    <span>9</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 10:00-10:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="10" data-position="11" value="11" class="sr-only">
                                                    <span>10</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 11:00-11:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="11" data-position="12" value="12" class="sr-only">
                                                    <span>11</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 12:00-12:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="12" data-position="13" value="13" class="sr-only">
                                                    <span>12</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 13:00-13:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="13" data-position="14" value="14" class="sr-only">
                                                    <span>13</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 14:00-14:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="14" data-position="15" value="15" class="sr-only">
                                                    <span>14</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 15:00-15:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="15" data-position="16" value="16" class="sr-only">
                                                    <span>15</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 16:00-16:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="16" data-position="17" value="17" class="sr-only">
                                                    <span>16</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 17:00-17:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="17" data-position="18" value="18" class="sr-only">
                                                    <span>17</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 18:00-18:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="18" data-position="19" value="19" class="sr-only">
                                                    <span>18</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 19:00-19:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="19" data-position="20" value="20" class="sr-only">
                                                    <span>19</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 20:00-20:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="20" data-position="21" value="21" class="sr-only">
                                                    <span>20</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 21:00-21:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="21" data-position="22" value="22" class="sr-only">
                                                    <span>21</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 22:00-22:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="22" data-position="23" value="23" class="sr-only">
                                                    <span>22</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Mi 23:00-23:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="23" data-position="24" value="24" class="sr-only">
                                                    <span>23</span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="schedule__weekday">Do</span></td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 00:00-00:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="0" data-position="1" value="1" class="sr-only">
                                                    <span>0</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 01:00-01:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="1" data-position="2" value="2" class="sr-only">
                                                    <span>1</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 02:00-02:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="2" data-position="3" value="3" class="sr-only">
                                                    <span>2</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 03:00-03:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="3" data-position="4" value="4" class="sr-only">
                                                    <span>3</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 04:00-04:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="4" data-position="5" value="5" class="sr-only">
                                                    <span>4</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 05:00-05:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="5" data-position="6" value="6" class="sr-only">
                                                    <span>5</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 06:00-06:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="6" data-position="7" value="7" class="sr-only">
                                                    <span>6</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 07:00-07:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="7" data-position="8" value="8" class="sr-only">
                                                    <span>7</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 08:00-08:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="8" data-position="9" value="9" class="sr-only">
                                                    <span>8</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 09:00-09:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="9" data-position="10" value="10" class="sr-only">
                                                    <span>9</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 10:00-10:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="10" data-position="11" value="11" class="sr-only">
                                                    <span>10</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 11:00-11:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="11" data-position="12" value="12" class="sr-only">
                                                    <span>11</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 12:00-12:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="12" data-position="13" value="13" class="sr-only">
                                                    <span>12</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 13:00-13:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="13" data-position="14" value="14" class="sr-only">
                                                    <span>13</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 14:00-14:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="14" data-position="15" value="15" class="sr-only">
                                                    <span>14</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 15:00-15:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="15" data-position="16" value="16" class="sr-only">
                                                    <span>15</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 16:00-16:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="16" data-position="17" value="17" class="sr-only">
                                                    <span>16</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 17:00-17:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="17" data-position="18" value="18" class="sr-only">
                                                    <span>17</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 18:00-18:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="18" data-position="19" value="19" class="sr-only">
                                                    <span>18</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 19:00-19:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="19" data-position="20" value="20" class="sr-only">
                                                    <span>19</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 20:00-20:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="20" data-position="21" value="21" class="sr-only">
                                                    <span>20</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 21:00-21:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="21" data-position="22" value="22" class="sr-only">
                                                    <span>21</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 22:00-22:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="22" data-position="23" value="23" class="sr-only">
                                                    <span>22</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Do 23:00-23:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="23" data-position="24" value="24" class="sr-only">
                                                    <span>23</span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="schedule__weekday">Fr</span></td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 00:00-00:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="0" data-position="1" value="1" class="sr-only">
                                                    <span>0</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 01:00-01:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="1" data-position="2" value="2" class="sr-only">
                                                    <span>1</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 02:00-02:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="2" data-position="3" value="3" class="sr-only">
                                                    <span>2</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 03:00-03:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="3" data-position="4" value="4" class="sr-only">
                                                    <span>3</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 04:00-04:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="4" data-position="5" value="5" class="sr-only">
                                                    <span>4</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 05:00-05:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="5" data-position="6" value="6" class="sr-only">
                                                    <span>5</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 06:00-06:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="6" data-position="7" value="7" class="sr-only">
                                                    <span>6</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 07:00-07:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="7" data-position="8" value="8" class="sr-only">
                                                    <span>7</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 08:00-08:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="8" data-position="9" value="9" class="sr-only">
                                                    <span>8</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 09:00-09:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="9" data-position="10" value="10" class="sr-only">
                                                    <span>9</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 10:00-10:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="10" data-position="11" value="11" class="sr-only">
                                                    <span>10</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 11:00-11:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="11" data-position="12" value="12" class="sr-only">
                                                    <span>11</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 12:00-12:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="12" data-position="13" value="13" class="sr-only">
                                                    <span>12</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 13:00-13:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="13" data-position="14" value="14" class="sr-only">
                                                    <span>13</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 14:00-14:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="14" data-position="15" value="15" class="sr-only">
                                                    <span>14</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 15:00-15:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="15" data-position="16" value="16" class="sr-only">
                                                    <span>15</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 16:00-16:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="16" data-position="17" value="17" class="sr-only">
                                                    <span>16</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 17:00-17:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="17" data-position="18" value="18" class="sr-only">
                                                    <span>17</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 18:00-18:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="18" data-position="19" value="19" class="sr-only">
                                                    <span>18</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 19:00-19:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="19" data-position="20" value="20" class="sr-only">
                                                    <span>19</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 20:00-20:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="20" data-position="21" value="21" class="sr-only">
                                                    <span>20</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 21:00-21:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="21" data-position="22" value="22" class="sr-only">
                                                    <span>21</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 22:00-22:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="22" data-position="23" value="23" class="sr-only">
                                                    <span>22</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Fr 23:00-23:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="23" data-position="24" value="24" class="sr-only">
                                                    <span>23</span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span class="schedule__weekday">Sa</span></td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 00:00-00:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="0" data-position="1" value="1" class="sr-only">
                                                    <span>0</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 01:00-01:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="1" data-position="2" value="2" class="sr-only">
                                                    <span>1</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 02:00-02:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="2" data-position="3" value="3" class="sr-only">
                                                    <span>2</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 03:00-03:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="3" data-position="4" value="4" class="sr-only">
                                                    <span>3</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 04:00-04:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="4" data-position="5" value="5" class="sr-only">
                                                    <span>4</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 05:00-05:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="5" data-position="6" value="6" class="sr-only">
                                                    <span>5</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 06:00-06:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="6" data-position="7" value="7" class="sr-only">
                                                    <span>6</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 07:00-07:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="7" data-position="8" value="8" class="sr-only">
                                                    <span>7</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 08:00-08:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="8" data-position="9" value="9" class="sr-only">
                                                    <span>8</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 09:00-09:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="9" data-position="10" value="10" class="sr-only">
                                                    <span>9</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 10:00-10:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="10" data-position="11" value="11" class="sr-only">
                                                    <span>10</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 11:00-11:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="11" data-position="12" value="12" class="sr-only">
                                                    <span>11</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 12:00-12:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="12" data-position="13" value="13" class="sr-only">
                                                    <span>12</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 13:00-13:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="13" data-position="14" value="14" class="sr-only">
                                                    <span>13</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 14:00-14:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="14" data-position="15" value="15" class="sr-only">
                                                    <span>14</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 15:00-15:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="15" data-position="16" value="16" class="sr-only">
                                                    <span>15</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 16:00-16:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="16" data-position="17" value="17" class="sr-only">
                                                    <span>16</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 17:00-17:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="17" data-position="18" value="18" class="sr-only">
                                                    <span>17</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 18:00-18:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="18" data-position="19" value="19" class="sr-only">
                                                    <span>18</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 19:00-19:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="19" data-position="20" value="20" class="sr-only">
                                                    <span>19</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 20:00-20:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="20" data-position="21" value="21" class="sr-only">
                                                    <span>20</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 21:00-21:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="21" data-position="22" value="22" class="sr-only">
                                                    <span>21</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 22:00-22:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="22" data-position="23" value="23" class="sr-only">
                                                    <span>22</span>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="schedule__item">
                                                    <input aria-label="Sa 23:00-23:59" role="checkbox" type="checkbox" name="days[]" data-day="0" data-hour="23" data-position="24" value="24" class="sr-only">
                                                    <span>23</span>
                                                </label>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <div class="content__group">

                                <div class="inline mb_20">
                                    <div class="inline__left">
                                        <h4>Globale Einstellungen</h4>
                                    </div>
                                    <div class="inline__right"></div>
                                </div>

                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <label class="form_label">Name<span class="color_red">*</span></label>
                                    </div>
                                    <div class="inline__right">
                                        <div class="form_wrap">
                                            <i-select v-model="model1" placeholder="11.12.129.21">
                                                <i-option v-for="item in cityList" :value="item.value" :key="item.value">{{ item.label }}</i-option>
                                            </i-select>
                                        </div>
                                    </div>
                                </div>

                                <div class="inline mb_20">
                                    <div class="inline__left">
                                        <h4>Zugang</h4>
                                    </div>
                                    <div class="inline__right"></div>
                                </div>



                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <div class="form_wrap"></div>
                                        <label class="form_label">VO ID</label>
                                    </div>
                                    <div class="inline__right">
                                        <div class="form_wrap">
                                            <i-input placeholder=""/>
                                        </div>
                                    </div>
                                </div>

                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <label class="form_label">User ID</label>
                                    </div>
                                    <div class="inline__right">
                                        <div class="form_wrap">
                                            <i-input  placeholder=""/>
                                        </div>
                                    </div>
                                </div>

                                <div class="inline form-group">
                                    <div class="inline__left">
                                        <label class="form_label">Passwort</label>
                                    </div>
                                    <div class="inline__right">
                                        <div class="form_wrap">
                                            <div class="form_wrap__input">
                                                <i-input type="password" placeholder=""/>
                                            </div>
                                            <span class="btn_pass_view"></span>
                                            <div class="form_wrap__message">
                                                <span class="form_info form_info__error">Einzigartig</span>
                                                <span class="form_info form_info__good">Ab 6 Zeichen</span>
                                                <span class="form_info">Kleinbuchstaben</span>
                                                <span class="form_info">Großbuchstaben</span>
                                                <span class="form_info">Titel</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="content__group">
                                <div class="inline mb_20">
                                    <div class="inline__left">
                                    </div>
                                    <div class="inline__right">
                                        <div class="text_right">
                                            <i-button type="primary" size="large">Anlegen</i-button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>

                </div>

            </section>


            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


        <script>

            var Main = {
                data () {
                    return {
                        vertical: 'Jetzt',

                        cityList: [
                            {
                                value: '11.12.129.21',
                                label: '11.12.129.21'
                            },
                            {
                                value: '11.12.129.22',
                                label: '11.12.129.22'
                            },
                            {
                                value: '11.12.129.23',
                                label: '11.12.129.23'
                            },
                            {
                                value: '11.12.129.24',
                                label: '11.12.129.24'
                            },
                            {
                                value: '11.12.129.11',
                                label: '11.12.129.11'
                            }
                        ],
                        model1: ''
                    }
                }
            };

            var Component = Vue.extend(Main);
            new Component().$mount('#app')



        </script>

    </body>

</html>
