<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <h1>Liste der Organisationen und Statistiken zu Mitarbeitern für heute</h1>
                    </div>





                    <div id="app">

                        <div class="form_inline mb_20" style="max-width: 460px;">
                            <div class="form_inline__elem selection__elem_select">
                                <i-select v-model="model1" size="large" placeholder="VP - Alle">
                                    <i-option v-for="item in cityList" :value="item.value" :key="item.value">{{ item.label }}</i-option>
                                </i-select>
                            </div>
                            <div class="form_inline__elem form_inline__elem_btn">
                                <i-button shape="circle">SUCHE</i-button>
                            </div>
                        </div>

                        <div class="data">
                            <ul class="data__views">
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="25" checked="">
                                        <span><i>25</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="50">
                                        <span><i>50</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="100">
                                        <span><i>100</i></span>
                                    </label>
                                </li>
                            </ul>
                            <div class="data__table">
                                <i-table :columns="columns1" :data="data1" ref="table" ></i-table>
                            </div>

                            <page :total="100" />

                        </div>

                    </div>

                </div>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>


            var table = {
                data () {
                    return {
                        columns1: [
                            {
                                "title": "ID",
                                "key": "id",
                                "fixed": "left",
                                "width": 125,
                                "className": "col_icon"
                            },
                            {
                                "title": "Name",
                                "key": "name",
                                "minWidth" : 180,
                                "sortable": true
                            },
                            {
                                "title": "VP",
                                "key": "vp",
                                "minWidth" : 120,
                                "sortable": false
                            },
                            {
                                "title": "Tageslimit  Vodafone",
                                "key": "limit",
                                "className": "text_center bg_rose",
                                "width": 180,
                                "sortable": false
                            },
                            {
                                "title": "Anfragen Vodafone",
                                "key": "request",
                                "className": "text_center",
                                "width": 180,
                                "sortable": false
                            },
                            {
                                "title": "Gesamtanzahl",
                                "key": "count",
                                "className": "text_center",
                                "width": 160,
                                "sortable": false
                            },
                            {
                                "title": "Erflog",
                                "key": "success",
                                "className": "color_green text_center",
                                "width": 100,
                                "sortable": false
                            },
                            {
                                "title": "Absage",
                                "key": "failure",
                                "className": "color_purple text_center",
                                "width": 120,
                                "sortable": false
                            },
                            {
                                "title": "Unbekannt",
                                "key": "unknown",
                                "className": "color_brown text_center",
                                "width": 150,
                                "sortable": false
                            }
                        ],
                        data1: [
                            {
                                "id": 129,
                                "name": "Vidofone-admin",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 198,
                                "count": 55,
                                "success": 10,
                                "failure": 30,
                                "unknown": 15,
                                cellClassName: {
                                    id: 'table_icon__admin',
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 130,
                                "name": "Vidofone-admin",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 215,
                                "count": 42,
                                "success": 12,
                                "failure": 25,
                                "unknown": 11,
                                cellClassName: {
                                    id: 'table_icon__su',
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 132,
                                "name": "VP 1-admin",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 314,
                                "count": 28,
                                "success": 7,
                                "failure": 29,
                                "unknown": 18,
                                cellClassName: {
                                    id: 'table_icon__headphone',
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 133,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                cellClassName: {
                                    id: 'table_icon__headphone',
                                    name: 'color_gray text_gray_underline'
                                }
                            },
                            {
                                "id": 187,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                cellClassName: {
                                    id: 'table_icon__headphone',
                                    name: 'color_gray text_gray_underline'
                                }
                            },
                            {
                                "id": 138,
                                "name": "MA 6",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 512,
                                "count": 84,
                                "success": 31,
                                "failure": 28,
                                "unknown": 42,
                                cellClassName: {
                                    id: 'table_icon__headphone',
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 152,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                cellClassName: {
                                    id: 'table_icon__headphone',
                                    name: 'color_gray text_gray_underline'
                                }
                            },
                            {
                                "id": 154,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                cellClassName: {
                                    id: 'table_icon__headphone',
                                    name: 'color_gray text_gray_underline'
                                }
                            },
                            {
                                "id": 159,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                cellClassName: {
                                    id: 'table_icon__headphone',
                                    name: 'color_gray text_gray_underline'
                                }
                            },
                            {
                                "id": 161,
                                "name": "MA 4",
                                "vp": "'VP 1",
                                "limit": "'198",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                cellClassName: {
                                    id: 'table_icon__headphone',
                                    name: 'color_gray text_gray_underline'
                                }
                            },
                        ],

                        cityList: [
                            {
                                value: 'VP - Alle',
                                label: 'VP - Alle'
                            },
                            {
                                value: 'VP - 1',
                                label: 'VP - 1'
                            },
                            {
                                value: 'VP - 2',
                                label: 'VP - 2'
                            },
                            {
                                value: 'VP - 3',
                                label: 'VP - 3'
                            },
                            {
                                value: 'VP - 4',
                                label: 'VP - 4'
                            },
                            {
                                value: 'VP - 5',
                                label: 'VP - 5'
                            }
                        ],
                        model1: ''
                    }
                }
            };


            var component = Vue.extend(table);
            new component().$mount('#app');



        </script>

    </body>
</html>
