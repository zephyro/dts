<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">
                <div class="container">

                    <div class="heading">
                        <div class="heading__col">
                            <ul class="breadcrumb">
                                <li><a href="#">Statistik</a></li>
                                <li><span>VP 1</span></li>
                            </ul>
                        </div>
                        <div class="heading__col"></div>
                    </div>





                    <div id="app">

                        <div class="selection">
                            <div class="selection__elem selection__elem_select">
                                <i-select v-model="model1" size="large" placeholder="VP - Alle">
                                    <i-option v-for="item in cityList" :value="item.value" :key="item.value">{{ item.label }}</i-option>
                                </i-select>
                            </div>
                            <div class="selection__elem">
                                <div class="date_interval">
                                    <div class="date_interval__label">Datum von</div>
                                    <div class="date_interval__content">
                                        <div class="date_interval__input">
                                            <date-picker type="date" size="small" confirm placeholder="Select date"></date-picker>
                                        </div>
                                        <div class="date_interval__text">bis</div>
                                        <div class="date_interval__input">
                                            <date-picker type="date" size="small" confirm placeholder="Select date"></date-picker>
                                        </div>
                                    </div>
                                    <div class="date_interval__button">
                                        <i-button size="small" shape="circle">Absenden</i-button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="data">
                            <ul class="data__views">
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="25" checked="">
                                        <span><i>25</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="50">
                                        <span><i>50</i></span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" name="content_views" value="100">
                                        <span><i>100</i></span>
                                    </label>
                                </li>
                            </ul>
                            <div class="data__table">
                                <i-table :columns="columns1" :data="data1" ref="table" ></i-table>
                            </div>

                            <page :total="100" />

                        </div>

                    </div>

                </div>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>


        <script>


            var table = {
                data () {
                    return {
                        columns1: [
                            {
                                "title": "ID",
                                "key": "id",
                                "fixed": "left",
                                "width": 78
                            },
                            {
                                "title": "Name",
                                "key": "name",
                                "minWidth" : 180,
                                "sortable": true
                            },
                            {
                                "title": "Datum",
                                "key": "date",
                                "width": 200,
                                "sortable": false
                            },
                            {
                                "title": "Anfragen Vodafone",
                                "key": "request",
                                "className": "text_center",
                                "width": 180,
                                "sortable": false
                            },
                            {
                                "title": "Gesamtanzahl",
                                "key": "count",
                                "className": "text_center",
                                "width": 160,
                                "sortable": false
                            },
                            {
                                "title": "Erflog",
                                "key": "success",
                                "className": "color_green text_center",
                                "width": 100,
                                "sortable": false
                            },
                            {
                                "title": "Absage",
                                "key": "failure",
                                "className": "color_purple text_center",
                                "width": 120,
                                "sortable": false
                            },
                            {
                                "title": "Unbekannt",
                                "key": "unknown",
                                "className": "color_brown text_center",
                                "width": 150,
                                "sortable": false
                            }
                        ],
                        data1: [
                            {
                                "id": 129,
                                "name": "Nutzer 1",
                                "date": "'15/11/2019 - 20/11/2019",
                                "request": 198,
                                "count": 55,
                                "success": 10,
                                "failure": 30,
                                "unknown": 15,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 130,
                                "name": "Nutzer 2",
                                "date": "'15/11/2019 - 20/11/2019",
                                "request": 215,
                                "count": 42,
                                "success": 12,
                                "failure": 25,
                                "unknown": 11,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 132,
                                "name": "Nutzer 3",
                                "date": "'15/11/2019 - 20/11/2019",
                                "request": 314,
                                "count": 28,
                                "success": 7,
                                "failure": 29,
                                "unknown": 18,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 133,
                                "name": "Nutzer 4",
                                "date": "'15/11/2019 - 20/11/2019",
                                "request": 315,
                                "count": 64,
                                "success": 18,
                                "failure": 48,
                                "unknown": 21,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 138,
                                "name": "Nutzer 5",
                                "date": "'15/11/2019 - 20/11/2019",
                                "request": 512,
                                "count": 84,
                                "success": 31,
                                "failure": 28,
                                "unknown": 42,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 140,
                                "name": "Nutzer 6",
                                "date": "'15/11/2019 - 20/11/2019",
                                "request": 425,
                                "count": 81,
                                "success": 52,
                                "failure": 11,
                                "unknown": 24,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 144,
                                "name": "Nutzer 7",
                                "date": "'15/11/2019 - 20/11/2019",
                                "request": 358,
                                "count": 91,
                                "success": 54,
                                "failure": 11,
                                "unknown": 21,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline'
                                }
                            },
                            {
                                "id": 145,
                                "name": "Nutzer 8",
                                "date": "'15/11/2019 - 20/11/2019",
                                "request": 221,
                                "count": 32,
                                "success": 15,
                                "failure": 11,
                                "unknown": 18,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline'
                                }
                            },

                            {
                                "id": 18,
                                "name": "Nutzer 9",
                                "date": "'15/11/2019 - 20/11/2019",
                                "request": 328,
                                "count": 44,
                                "success": 21,
                                "failure": 14,
                                "unknown": 12,
                                cellClassName: {
                                    name: 'color_blue text_blue_underline'
                                }
                            }
                        ],

                        cityList: [
                            {
                                value: 'VP - Alle',
                                label: 'VP - Alle'
                            },
                            {
                                value: 'VP - 1',
                                label: 'VP - 1'
                            },
                            {
                                value: 'VP - 2',
                                label: 'VP - 2'
                            },
                            {
                                value: 'VP - 3',
                                label: 'VP - 3'
                            },
                            {
                                value: 'VP - 4',
                                label: 'VP - 4'
                            },
                            {
                                value: 'VP - 5',
                                label: 'VP - 5'
                            }
                        ],
                        model1: ''
                    }
                }
            };


            var component = Vue.extend(table);
            new component().$mount('#app');



        </script>

    </body>

</html>
