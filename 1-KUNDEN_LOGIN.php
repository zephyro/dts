<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body  class="auth_bg">

        <div class="auth" id="app">

            <div class="auth__wrap">

                <div class="auth__logo">
                    <img src="img/logo_large.svg" class="img-fluid" alt="">
                </div>

                <div class="auth__form">
                    <div class="auth__form_wrap">
                        <div class="auth__title"><span>KUNDEN LOGIN</span></div>

                        <i-form>
                            <form-item prop="user">
                                <i-input type="text"  placeholder="Email">
                                    <icon type="ios-mail" slot="prepend"></icon>
                                </i-input>
                            </form-item>
                            <form-item prop="password">
                                <i-input type="password"  placeholder="Password">
                                    <icon type="ios-lock-outline" slot="prepend"></icon>
                                </i-input>
                            </form-item>
                            <form-item>
                                <i-button type="primary" @click="handleSubmit('formInline')">login</i-button>
                            </form-item>
                        </i-form>

                        <div class="auth__error">Login nicht möglich</div>

                    </div>
                </div>

            </div>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            var table = {
                data () {
                    return {
                        list: [
                            {
                                value: 'VP - Alle',
                                label: 'VP - Alle'
                            },
                            {
                                value: 'VP - 1',
                                label: 'VP - 1'
                            },
                            {
                                value: 'VP - 2',
                                label: 'VP - 2'
                            },
                            {
                                value: 'VP - 3',
                                label: 'VP - 3'
                            },
                            {
                                value: 'VP - 4',
                                label: 'VP - 4'
                            },
                            {
                                value: 'VP - 5',
                                label: 'VP - 5'
                            }
                        ],
                        model1: ''
                    }
                }
            };

            var component = Vue.extend(table);
            new component().$mount('#app');

        </script>

    </body>
</html>
